# How to install Kali Nethunter on OnePlus One

1. [Download](#download)
2. [Unlocking & TWRP](#unlocking-twrp)
3. [Installation](#installation)
4. [BOOT!](#boot)



## Download
If your phone is stock then you need to unlock it and install TWRP. Grab the latest image here: https://eu.dl.twrp.me/bacon/
<br>You probably also need to have fastboot&adb commands on your computer: `sudo apt-get install android-tools-adb android-tools-fastboot`
<br>Then download a ROM (Lollipop or Marshmellow), I use CyanogenMod 13 from here: https://web.archive.org/web/20170108154545/http://oss.reflected.net/jenkins/175145/cm-13.0-20160819-SNAPSHOT-ZNH5YAO0IN-bacon.zip
<br>AFAIK it is the latest availabe official CM13 version. (COS13 can be downloaded here: https://web.archive.org/web/20170115211239/http://builds.cyngn.com/factory/bacon/cm-13.1.2-ZNH2KAS3P0-bacon-signed-8502142fdc.zip)
<br>Go here https://build.nethunter.com/nightly/ and download the latest general kali file system (nethunter-generic-armhf-kalifs-full-....) and the device specific kernel (kernel-nethunter-oneplus1-...). There are two versions provided, for Lollipop and Marshmellow.
<br><br>

## Unlocking & TWRP
go into fastboot mode (poweroff -> power+vol-up)
<br>connect phone to computer
<br>run `fastboot oem unlock`
<br>`fastboot flash recovery twrp.img`
<br><br>

## Installation
Reboot to recovery (poweroff -> power+vol-down)
<br>go to wipe -> advanced -> select everything except OTG. Perform the wipe
<br>from computer copy all other files to the phone.
<br>Install -> select your ROM and flash it. Boot and finish setup wizzard
<br>reboot to recovery and install the kali file system and after that the kali kernel. Boot.
<br>You may want to install support for swipe-typing. Download and flash this CM13 zip: https://github.com/Micha-Btz/swipelib/releases/tag/0.3.1 (It installes a [.so lib](https://gitlab.opengapps.org/opengapps/arm64/blob/master/lib64/23/libjni_latinimegoogle.so))
<br>Also really nice is to install the ColorOS camera apk and flash a zip for manual focus: https://forum.xda-developers.com/oneplus-one/themes-apps/port-coloros-camera-cm11s-t3030369
<br>Plugins can be found here: https://www.dropbox.com/sh/jumlgpph14hwhk1/AAAZwjCfV19njaFHLFWxtnW-a?dl=0
<br><br>

## BOOT!
Open Nethunter app and let it comfigure.
<br>Open Nethunter Terminal with a Kali shell and run apt-get updates && apt-get upgrades -y
<br>It will update hostadp, enter N
<br>Download Nethunter store from https://store.nethunter.com/NetHunterStore.apk and install it
<br>Open and go to updates tab. Update shown apps, and get the apps you want...
<br>

Youre done :)